
# Standart Craft

Basic quality stories


## Mandatory

Toutes les règles suivantes sont obligatoires sauf nécessitée.


### Defect analysis

Tout défaut trouvé hors développement doit faire l'objet d'une analyse sur les points suivants:

1. Comment améliorer notre stratégie de tests ?
2. Comment améliorer nos pratiques techniques ?
3. Comment améliorer nos méthodes ?


### Pair Programming

Deux développeurs qui travaillent sur le même code en même temps.

Cela va être difficile et on s'améliore dans le temps.
Pour tous les développements.
- Définir les créneaux de travail (ex 2H le matin, 3H l'après midi).
- Rotation des binômes définitions le matin et l'après midi.
- Pas de binômes junior-junior.
- Pas de mail/téléphone/citadelle.
- Être vigilant sur le niveau de concentration.


Recommandations: mob programming


### Test unitaires

Les développements doivent avoir des tests unitaires. C'est à dire qui s'executent sans IO et rapidement ~1ms.

Recommandations:
- Tests unitaires fonctionnels
- TDD


### Clean architecture

Séparer le code métier du code technique.

Recommandations:
- Hexagonal architecture
- Domain Driven Design, tactical patterns


## Recommandations


### Intégration continue et trunk based


###
