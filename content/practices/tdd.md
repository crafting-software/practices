---
title: "TDD - Test Driven Developement"
lang: fr
tags:
  - testing
objectives: |
  - Fluidité de la production de code
  - Qualité du code
  - Vitesse de production

---

1. Write a failing test
2. Make it pass
3. Refactor
